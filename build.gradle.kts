plugins {
    kotlin("jvm") version "1.9.22"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "com.github.johnrengelman.shadow")

    kotlin {
        jvmToolchain(18)
    }

    val exposedVersion: String by project
    val mssqlServerVersion: String by project
    dependencies {
        /* Database management */
        implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
        implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
        implementation("com.microsoft.sqlserver:mssql-jdbc:$mssqlServerVersion")
    }
}

tasks.jar {
    enabled = false
}

tasks.shadowJar {
    enabled = false
}

kotlin {
    jvmToolchain(18)
}