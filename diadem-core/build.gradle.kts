repositories {
    mavenCentral()
}

tasks.shadowJar {
    manifest {
        attributes["Main-Class"] = "me.trup10ka.diadem.MainKt"
    }
    archiveBaseName = "diadem-core"
}