package me.trup10ka.diadem.template

interface TemplateLoader
{
    fun loadTemplate(filePath: String): Template
}