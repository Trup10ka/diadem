package me.trup10ka.diadem.template

import java.io.File

object FileConnectionTemplateLoader : TemplateLoader
{
    override fun loadTemplate(filePath: String): Template
    {
        val fileContent = File(filePath).readText()
        return parseTemplate(fileContent)
    }

    private fun parseTemplate(fileContent: String): Template
    {
        val configMap = fileContent.split("\n")
            .map { it.split("=") }
            .associate { it[0].trim() to it[1].trim('"', ' ', '\r') }
        
        return Template(
            name = configMap["name"],
            username = configMap["username"] ?: throw IllegalArgumentException("Username missing in configuration file"),
            password = configMap["password"] ?: throw IllegalArgumentException("Password missing in configuration file"),
            url = configMap["url"] ?: throw IllegalArgumentException("Url missing in configuration file"),
            port = configMap["port"]?.toInt(),
            driver = configMap["driver"] ?: throw IllegalArgumentException("Driver missing in configuration file"),
            dbName = configMap["dbName"]
        )
    }
}
