package me.trup10ka.diadem.template

import me.trup10ka.diadem.database.driver.DriverMapper

data class Template(
    val name: String? = null,
    val username: String,
    val password: String,
    val url: String,
    val port: Int? = null,
    val driver: String,
    val dbName: String? = null
)
{
    val fullUrl: String
        get()
        {
            var url = "jdbc:${DriverMapper.resolveUrlDriver(driver)}://$url"

            if (port != null)
                url = "$url:$port"
            if (dbName != null)
                url = "$url;database=$dbName"
            return url
        }
}
