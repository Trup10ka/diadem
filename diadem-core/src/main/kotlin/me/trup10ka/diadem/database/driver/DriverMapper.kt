package me.trup10ka.diadem.database.driver

object DriverMapper
{
    fun resolveUrlDriver(driver: String): String
    {
        val dbDriver = DbDriver.entries.find { it.driverName == driver }

        return dbDriver?.urlForm ?: throw IllegalArgumentException("Driver: '$driver' not found")
    }
}