package me.trup10ka.diadem.database.driver

enum class DbDriver(
    val driverName: String,
    val urlForm: String)
{
    MYSQL("com.mysql.cj.jdbc.Driver", "mysql"),
    ORACLE("oracle.jdbc.OracleDrive", "oracle:thin"),
    SQL_SERVER("com.microsoft.sqlserver.jdbc.SQLServerDriver", "sqlserver")
}